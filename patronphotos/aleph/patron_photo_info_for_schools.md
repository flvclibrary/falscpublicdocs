# Patron photo information for colleges and universities.

## Workflow for adding Patron Photos to Aleph patron accounts.
* Aleph patron photos cannot be added as part of the patron load.  They must be loaded separately. 
* Each patron photo file must be given a filename which matches the patron id.  See specs below.
* To start the process, please submit a case to the helpdesk.  FLVC will provide you with a secure username/password and location for you to upload the file of images.
 
## Photo specs and workflow:
* Institutions assemble pics
  * Available formats include: .jpg, .jpeg, .gif, .bmp, .jif
  * Use a width/height ratio of 1:1.  The filesize should not be larger than 110kb.
  * Each patron photo should be saved as a separate file.  The filename should match the patron id, in the format XXXXX.jpg.
  * zip all pics into one zip file

## File naming convention

Use the following naming convention for the zip file:
patronpics_{inst3}_{dev|test|prod}_{seq}_yyyymmdd.zip

See the table at the bottom of this document for inst3 values.

## File transfer

* Upload zipped file of patron photos to securefiles.flvc.org server.  
  * Use sftp to transfer the file to securefiles.flvc.org.
* User requests login by submitting a case with Help Desk (https://www.flvc.org/help-center)
* User sftp's zip file to securefiles.flvc.org

## FLVC processes
* Automated process will move the zip file to the aleph server.
* Match Patron with pic  
  * unzip file and run match process
  * Patron ID matching:  Aleph uses the patron type 00 (system id) to display the patron photo in his/her account in the Circulation client under Global Patron Information.
  * Most libraries will have the patron type 03 (FCS) or 02 (SULs).  FLVC will run a script to match the ID you provide to the 00 (System ID) type used by Aleph and change the image filename to the 00 type filename.
* Aleph’s patron purge job, which removes patron accounts that do not have outstanding debts, loans or requests, based on expiration date, will also remove the patron image file.
 

### Inst3 values

| Institution | Inst3 |
| ----------- |:-----:|
|Broward College|BOC|
|Chipola College|CJC|
|College of Central Florida|CCC|
|Daytona State College|DBC|
|Eastern Florida State College|BEC|
|Florida Academic Repository|FLR|
|Florida Agricultural and Mechanical University|AMU|
|Florida Atlantic University|FAU|
|Florida Gateway College|LCC|
|Florida Gulf Coast University|GCU|
|Florida International University|FIU|
|Florida Keys Community College|FKC|
|Florida Polytechnic University|FPU|
|Florida SouthWestern State College|ECC|
|Florida State College at Jacksonville|FJC|
|Florida State University|FSU|
|Gulf Coast State College|GCC|
|Hillsborough Community College|HCC|
|Indian River State College|IRC|
|Lake-Sumter State College|LSC|
|Miami Dade College|MDC|
|New College of Florida|NCU|
|North Florida Community College|NJC|
|Northwest Florida State College|OWC|
|Palm Beach State College|PCC|
|Pasco-Hernando State College|PHC|
|Pensacola State College|PJC|
|Polk State College|PKC|
|Santa Fe College|SNC|
|Seminole State College|SCC|
|South Florida State College|SOC|
|St. Johns River State College|SSC|
|St. Petersburg College|SPC|
|State College of Florida, Manatee-Sarasota|MJC|
|Tallahassee Community College|TCC|
|University of Central Florida|CFU|
|University of Florida|UFU|
|University of North Florida|NFU|
|University of South Florida|SFU|
|University of West Florida|WFU|
|Valencia College|VCC|

